from nose.tools import assert_equal

import aims

def test_integers():
    mylist = [4,8,11,15]
    obs = aims.std(mylist) # testing for positive integers 
    exp = 4.03113
    assert_equal(obs, exp)


def test_negative():

    mylist = [-1,-2,-3,-4]
    obs = aims.std(mylist) # testing for negstive integers 
    exp = 1.11803
    assert_equal(obs, exp)

def test_floats_and_integers():

    mylist = [1,2.5,3,4.5]
    obs = aims.std(mylist)   # testing for a case where we have floats 
    exp = 1.25000
    assert_equal(obs, exp)

def test_myfiles():

    myfiles = ["data/bert/audioresult-00215","data/bert/audioresult-00215"]
    obs = aims.avg_range(myfiles)   # testing for a case where we have floats 
    exp = 5
    assert_equal(obs, exp)

