#!/usr/bin/env python
import numpy as np

def main():
    print 'Welcome to the AIMS module'

def std(mylist):
   try:
        average = np.average(mylist) # calculate the average of the list
        mysum = 0                    # initialises the sum to 0
        for y in mylist:             # loop through the list           
            d = y - average          # calculate the value of d, which will be used in the formula
            mysum = mysum + d**2     
        SD = np.sqrt(mysum/len(mylist))# calculate the standard deviation
        SD_final = round(SD,5)        # set the final value of standard deviation to five decimal places
   except ZeroDivisionError:
        raise ZeroDivisionError("You have put an empty list ")

   except TypeError:
        raise TypeError("That list is not a list of numbers")

   return SD_final
def avg_range(filelist):      # define a function
    try:
        rangelist = []        # start with an enpty list
        for file in filelist: # loop through the file list
            myfile = open(file)
            for line in myfile: # loop through the lines in a file
                if line.startswith("Range"):
                    newlist = [item.strip() for item in line.split(':')]
                    rangelist.append(int(newlist[1]))# get the value of the range in a file
            myfile.close()    
        Avg = np.average(rangelist) # get the average of the range in the range list
    except IOError:
        raise IOError("no such file or directory")
    return Avg
if __name__ == "__main__":
    main()
